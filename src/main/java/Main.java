import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.JSONPObject;
import java.io.File;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import model.Candy;
import parser.ParserJSON;
import parser.SchemaJSON;
import org.json.simple.JSONObject;

public class Main {


  private static void printCandyList(List<Candy> candies) {
    Comparator<Candy> comparator = Comparator.comparing(Candy::getEnergy);
    Collections.sort(candies, comparator);
    for (Candy c : candies) {
      System.out.println(c);
    }
  }


  public static void main(String[] args) throws Exception {
    final File json = new File("src\\main\\resources\\candy.json");
    final File schema = new File("src\\main\\resources\\candySchema.json");

    ParserJSON parcer = new ParserJSON();

    try {

      if (SchemaJSON.isValidateJSON(json, schema)) {
        printCandyList(parcer.getCandiesList(json));

      } else {
        System.out.println("invalid json");
      }


    } catch (Exception e) {
      System.out.println("Exception:" + e.getMessage());
    }

  }

}
