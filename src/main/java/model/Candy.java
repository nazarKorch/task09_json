package model;

import java.util.ArrayList;
import java.util.List;

public class Candy {

  private int num;
  private String nameOfCandy;
  private String typeOfCandy;
  private int energy;
  private String production;
  private List<String> ingredients;

  public Candy() {
  }

  public Candy(int num, String nameOfCandy, String typeOfCandy, int energy, String production,
      List<String> ingredients) {
    this.num = num;
    this.nameOfCandy = nameOfCandy;
    this.typeOfCandy = typeOfCandy;
    this.energy = energy;
    this.production = production;
    this.ingredients = ingredients;
  }

  public void setNum(int num) {
    this.num = num;
  }

  public void setNameOfCandy(String nameOfCandy) {
    this.nameOfCandy = nameOfCandy;
  }

  public void setTypeOfCandy(String typeOfCandy) {
    this.typeOfCandy = typeOfCandy;
  }

  public void setEnergy(int energy) {
    this.energy = energy;
  }

  public void setProduction(String production) {
    this.production = production;
  }

  public void setIngredients(List<String> ingredients) {
    this.ingredients = ingredients;
  }

  public List<String> getIngredients() {
    return ingredients;
  }

  public String getNameOfCandy() {
    return nameOfCandy;
  }

  public String getTypeOfCandy() {
    return typeOfCandy;
  }

  public String getProduction() {
    return production;
  }

  public int getNum(){
    return num;
  }

  public int getEnergy() {
    return energy;
  }

  public String toString() {

    return "Candy{num=" + getNum() +
        ", nameOfCandy=" + getNameOfCandy() +
        ", typeOfCandy=" + getTypeOfCandy() +
        ", energy=" + getEnergy() +
        ", production=" + getProduction() +
        ", ingredients=" + getIngredients() + "}";
  }

}

