package parser;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import model.Candy;

public class ParserJSON {

  private ObjectMapper objectMapper;

  public ParserJSON() {
    objectMapper = new ObjectMapper();
  }

  public List<Candy> getCandiesList(File json){
    List<Candy> candies = new ArrayList<Candy>();
    try{
      candies = Arrays.asList(objectMapper.readValue(json, Candy[].class));
    }catch (Exception e){
      System.out.println(e.getMessage());
    }
    return candies;
  }


}
