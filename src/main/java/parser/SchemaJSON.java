package parser;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.github.fge.jsonschema.main.JsonValidator;
import java.io.File;

public class SchemaJSON {

  public static boolean isValidateJSON(File jsonFile, File schemaFile) throws Exception {
    JsonNode data = JsonLoader.fromFile(jsonFile);
    JsonNode schema = JsonLoader.fromFile(schemaFile);
    JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
    JsonValidator validator = factory.getValidator();
    ProcessingReport report = validator.validate(data, schema);

    return report.isSuccess();

  }

}
